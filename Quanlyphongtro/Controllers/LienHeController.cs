﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quanlyphongtro.Data;
using Quanlyphongtro.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quanlyphongtro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LienHeController : ControllerBase
    {
        public readonly AppDbContext _context;


        public LienHeController(AppDbContext context)
        {
            _context = context;

        }
        // GET: api/<ProductsController>
        [HttpGet("DanhsachLienHe")]
        public async Task<ActionResult<IEnumerable<LienHe>>> GetLienHe()
        {
            var result = _context.LienHe.OrderByDescending(e => e.id_LienHe);
            return await result.ToListAsync();
        }

        // GET api/<ProductsController>/5
        [HttpGet("Danhsachphong/{id}")]
        public string Get(int id)
        {
            return "value";
        }



        // [Authorize(Roles = "Admin")]
        // POST api/<ProductsController>
        [HttpPost("themLienHe")]
        public async Task<ActionResult<LienHe>> themLienHe([FromBody] LienHe model)
        {

            _context.LienHe.Add(model);
            await _context.SaveChangesAsync();
            return CreatedAtAction("themLienHe", new { id = model.id_LienHe }, model);

        }

       
        // [Authorize(Roles = "Admin")]
        // DELETE api/<ProductsController>/5
        [HttpDelete("XoaLienHe/{id}")]
        public async Task<IActionResult> DeleteLienHe(int id)
        {
            var Phong = await _context.LienHe.FindAsync(id);
            if (Phong == null)
            {
                return NotFound();
            }

            _context.LienHe.Remove(Phong);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
