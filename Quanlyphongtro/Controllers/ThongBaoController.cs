﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quanlyphongtro.Data;
using Quanlyphongtro.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quanlyphongtro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThongBaoController : ControllerBase
    {
        public readonly AppDbContext _context;


        public ThongBaoController(AppDbContext context)
        {
            _context = context;

        }
        // GET: api/<ProductsController>
        [HttpGet("DanhsachThongBao")]
        public async Task<ActionResult<IEnumerable<ThongBao>>> GetProducts()
        {
            var result = _context.ThongBao.OrderByDescending(e => e.Id_ThongBao);
            return await result.ToListAsync();
        }
        [HttpGet("gioihanThongBao")]
        public async Task<ActionResult<IEnumerable<ThongBao>>> getthongbao()
        {
            var result = _context.ThongBao.Take(5).OrderByDescending(e => e.Id_ThongBao);
            return await result.ToListAsync();
        }

        // GET api/<ProductsController>/5
        [HttpGet("Danhsachthongbao/{id}")]
        public async Task<ActionResult<IEnumerable<ThongBao>>> GetProducts(int id)
        {
            var result = await _context.ThongBao.FirstOrDefaultAsync(c => c.Id_ThongBao == id);

            return Ok(result);
        }



        // [Authorize(Roles = "Admin")]
        // POST api/<ProductsController>
        [HttpPost("themthongbao")]
        public async Task<ActionResult<ThongBao>> themthongbao([FromBody] ThongBao model)
        {

            _context.ThongBao.Add(model);
            await _context.SaveChangesAsync();
            return CreatedAtAction("themthongbao", new { id = model.Id_ThongBao }, model);

        }

        //[Authorize(Roles = "Admin")]
        // PUT api/<ProductsController>/5
        [HttpPut("suathongbao/{id}")]

        public async Task<IActionResult> PutProducts(int id, ThongBao model)
        {
            if (id != model.Id_ThongBao)
            {
                return BadRequest();
            }

            _context.Entry(model).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool ProductsExists(int id)
        {
            return _context.ThongBao.Any(e => e.Id_ThongBao == id);
        }

        // [Authorize(Roles = "Admin")]
        // DELETE api/<ProductsController>/5
        [HttpDelete("XoaThongBao/{id}")]
        public async Task<IActionResult> DeleteThuePhong(int id)
        {
            var Phong = await _context.ThongBao.FindAsync(id);
            if (Phong == null)
            {
                return NotFound();
            }

            _context.ThongBao.Remove(Phong);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
