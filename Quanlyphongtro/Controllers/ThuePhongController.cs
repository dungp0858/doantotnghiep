﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quanlyphongtro.Data;
using Quanlyphongtro.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quanlyphongtro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThuePhongController : ControllerBase
    {
        public readonly AppDbContext _context;


        public ThuePhongController(AppDbContext context)
        {
            _context = context;

        }
        // GET: api/<ProductsController>
        [HttpGet("DanhsachThue")]
        public async Task<ActionResult<IEnumerable<ThuePhong>>> danhsachthue()
        {
            var result = _context.ThuePhong.OrderByDescending(e => e.Id);
            return await result.ToListAsync();
        }

        // GET api/<ProductsController>/5
        [HttpGet("Danhsachthue/{id}")]
        public async Task<ActionResult<IEnumerable<ThuePhong>>> GetProducts(int id)
        {
            var result = await _context.ThuePhong.FirstOrDefaultAsync(c => c.Id == id);

            return Ok(result);
        }


        [HttpGet("Danhsachthuebyuser/{userID}")]
        public async Task<ActionResult<IEnumerable<ThuePhong>>> danhsachthuebyUser(string userID)
        {
            var thuephongbyUser = _context.ThuePhong.Where(a=> a.UserID  == userID).OrderByDescending(i=>i.Id);

            return await thuephongbyUser.ToListAsync();
        }

        [HttpGet("thuebyuser/{userID}")]
        public async Task<ActionResult<IEnumerable<ThuePhong>>> thuebyUser(string userID )
        {
            var thuephongUser = _context.ThuePhong.Where(a => a.UserID == userID).OrderByDescending(i => i.Id).FirstOrDefaultAsync();

            return Ok( await thuephongUser);
        }

        [HttpGet("Danhsachthuechuadongtien")]
        public async Task<ActionResult<IEnumerable<ThuePhong>>> danhsachthuechuadongtien()
        {
            var thuephongbyUser = _context.ThuePhong.Where(a => a.IdTTThanhToan == 1).OrderByDescending(i => i.Id);

            return await thuephongbyUser.ToListAsync();
        }

        // [Authorize(Roles = "Admin")]
        // POST api/<ProductsController>
        [HttpPost("themthuephong")]
        public async Task<ActionResult<ThuePhong>> themthuephong([FromBody] ThuePhong model)
        {

            _context.ThuePhong.Add(model);
            await _context.SaveChangesAsync();
            return CreatedAtAction("themthuephong", new { id = model.Id}, model);

        }

        //[Authorize(Roles = "Admin")]
        // PUT api/<ProductsController>/5
        [HttpPut("suathuephong/{id}")]

        public async Task<IActionResult> PutProducts(int id, ThuePhong model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }

            _context.Entry(model).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool ProductsExists(int id)
        {
            return _context.ThuePhong.Any(e => e.Id_Phong == id);
        }

        // [Authorize(Roles = "Admin")]
        // DELETE api/<ProductsController>/5
        [HttpDelete("XoaThuePhong/{id}")]
        public async Task<IActionResult> DeleteThuePhong(int id)
        {
            var Phong = await _context.ThuePhong.FindAsync(id);
            if (Phong == null)
            {
                return NotFound();
            }

            _context.ThuePhong.Remove(Phong);
            await _context.SaveChangesAsync();

            return NoContent();
        }

    }
}
