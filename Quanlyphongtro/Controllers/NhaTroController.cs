﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quanlyphongtro.Data;
using Quanlyphongtro.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quanlyphongtro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NhaTroController : ControllerBase
    {
        public readonly AppDbContext _context;


        public NhaTroController(AppDbContext context)
        {
            _context = context;

        }
        // GET: api/<ProductsController>
        [HttpGet("DanhsachNha")]
        public async Task<ActionResult<IEnumerable<NhaTro>>> GetNha()
        {
            var result = _context.NhaTro.OrderByDescending(e => e.Id_NhaTro);
            return await result.ToListAsync();
        }

    
    }
}
