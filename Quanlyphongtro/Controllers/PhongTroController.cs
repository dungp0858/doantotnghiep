﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quanlyphongtro.Data;
using Quanlyphongtro.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quanlyphongtro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhongTroController : ControllerBase
    {
        public readonly AppDbContext _context;
       

        public PhongTroController(AppDbContext context)
        {
            _context = context;
            
        }
        // GET: api/<ProductsController>
        [HttpGet("Danhsachphong")]
        public async Task<ActionResult<IEnumerable<Phong>>> GetProducts()
        {
            var result = _context.Phong.OrderByDescending(e => e.Id_Phong);
            return await result.ToListAsync();
        }


        [HttpGet("Danhsachphongchuathue")]
        public async Task<ActionResult<IEnumerable<Phong>>> Danhsachphongchuathue()
        {
            var result = _context.Phong.Where(i=>i.MaTinhTrang == 1).OrderByDescending(e => e.Id_Phong);
            return await result.ToListAsync();
        }

        // GET api/<ProductsController>/5
        [HttpGet("Danhsachphong/{id}")]
        public string Get(int id)
        {
            return "value";
        }



        // [Authorize(Roles = "Admin")]
        // POST api/<ProductsController>
        [HttpPost("themphong")]
        public async Task<ActionResult<Phong>> themphong([FromBody] Phong model)
        {

            _context.Phong.Add(model);
            await _context.SaveChangesAsync();
            return CreatedAtAction("themphong", new { id = model.Id_Phong }, model);

        }

        //[Authorize(Roles = "Admin")]
        // PUT api/<ProductsController>/5
        [HttpPut("suaphong/{id}")]

        public async Task<IActionResult> PutProducts(int id, Phong model)
        {
            if (id != model.Id_Phong)
            {
                return BadRequest();
            }

            _context.Entry(model).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool ProductsExists(int id)
        {
            return _context.Phong.Any(e => e.Id_Phong == id);
        }

        // [Authorize(Roles = "Admin")]
        // DELETE api/<ProductsController>/5
        [HttpDelete("XoaPhong/{id}")]
        public async Task<IActionResult> DeletePhong(int id)
        {
            var Phong = await _context.Phong.FindAsync(id);
            if (Phong == null)
            {
                return NotFound();
            }

            _context.Phong.Remove(Phong);
            await _context.SaveChangesAsync();

            return NoContent();
        }

    }
}
