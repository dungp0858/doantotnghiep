﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quanlyphongtro.Data;
using Quanlyphongtro.Data.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Quanlyphongtro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChotDienNuocController : ControllerBase
    {
        public readonly AppDbContext _context;
        private readonly IWebHostEnvironment _env;


        public ChotDienNuocController(AppDbContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;

        }
        // GET: api/<ProductsController>
        [Authorize(Roles = "Admin")]
        [HttpGet("Danhsachchot")]
        public async Task<ActionResult<IEnumerable<ChotDienNuoc>>> GetChot()
        {
            var result = _context.ChotDienNuoc.OrderByDescending(e => e.Id_ChotDN);
            return await result.ToListAsync();
        }

        // GET api/<ProductsController>/5
        [Authorize(Roles = "Admin")]
        [HttpGet("Danhsachchotby/{id}")]
        public async Task<ActionResult<IEnumerable<ChotDienNuoc>>> Danhsachchotby(int id)
        {
            var result = await _context.ChotDienNuoc.FirstOrDefaultAsync(c => c.Id_ChotDN == id);

            return Ok(result);
        }



        // [Authorize(Roles = "Admin")]
        // POST api/<ProductsController>
        [Authorize(Roles = "User")]
        [HttpPost("guichot")]
        public async Task<ActionResult<ChotDienNuoc>> guichot([FromBody] ChotDienNuoc model)
        {

            _context.ChotDienNuoc.Add(model);
            await _context.SaveChangesAsync();
            return CreatedAtAction("guichot", new { id = model.Id_ChotDN }, model);

        }

       
         [Authorize(Roles = "Admin")]
        // DELETE api/<ProductsController>/5
        [HttpDelete("Xoachot/{id}")]
        public async Task<IActionResult> DeleteThuePhong(int id)
        {
            var Phong = await _context.ChotDienNuoc.FindAsync(id);
            if (Phong == null)
            {
                return NotFound();
            }

            _context.ChotDienNuoc.Remove(Phong);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [Route("SaveFile")]
        [HttpPost]
        public JsonResult SaveFile()
        {
            try
            {
                var httpRequest = Request.Form;
                var postedFile = httpRequest.Files[0];
                string filename = postedFile.FileName;
                var physicalPath = _env.ContentRootPath + "/Photos/" + filename;

                using (var stream = new FileStream(physicalPath, FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                }

                return new JsonResult(filename);
            }
            catch (Exception)
            {

                return new JsonResult("anonymous.png");
            }
        }
    }
}
