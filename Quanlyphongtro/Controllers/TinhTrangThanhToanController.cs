﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quanlyphongtro.Data;
using Quanlyphongtro.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quanlyphongtro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TinhTrangThanhToanController : ControllerBase
    {

        public readonly AppDbContext _context;


        public TinhTrangThanhToanController(AppDbContext context)
        {
            _context = context;

        }
        // GET: api/<ProductsController>
        [HttpGet("ListTinhTrang")]
        public async Task<ActionResult<IEnumerable<TTThanhToan>>> ListTinhTrang()
        {
            var result = _context.TTThanhToan.OrderByDescending(e => e.IdTTThanhToan);
            return await result.ToListAsync();
        }
    }
}
