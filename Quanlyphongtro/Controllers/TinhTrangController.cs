﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quanlyphongtro.Data;
using Quanlyphongtro.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quanlyphongtro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TinhTrangController : ControllerBase
    {
        public readonly AppDbContext _context;


        public TinhTrangController(AppDbContext context)
        {
            _context = context;

        }
        // GET: api/<ProductsController>
        [HttpGet("ListTinhTrang")]
        public async Task<ActionResult<IEnumerable<TinhTrang>>> ListTinhTrang()
        {
            var result = _context.TinhTrang.OrderByDescending(e => e.MaTinhTrang);
            return await result.ToListAsync();
        }
    }
}
