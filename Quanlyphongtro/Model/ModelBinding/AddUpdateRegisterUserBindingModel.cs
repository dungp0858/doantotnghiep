﻿using System;
using System.Collections.Generic;

namespace Quanlyphongtro.Model.ModelBinding
{
    public class AddUpdateRegisterUserBindingModel
    {
        public string Hovaten { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime NgayDangKy { get; set; }
        public int SoDT { get; set; }
        public string DiaChi { get; set; }
        public List<string> Roles { get; set; }
    }
}
