using System;
using System.Collections.Generic;

namespace Models.DTO
{
    public class UserDTO
    {

        public UserDTO(string id, string hovaten, string email, int sodt, DateTime ngaydangky,string diachi, List<string> roles)
        {   Id = id;
            Hovaten = hovaten;
            Email = email;
            SoDT = sodt;
            NgayDangKy = ngaydangky;
            Diachi = diachi;
            Roles = roles;
        }
        public string Id { get; set; }
        public string Hovaten { get; set; }
        public string Email { get; set; }
        public int SoDT { get; set; }
        public DateTime NgayDangKy { get; set; }
        public string Diachi { get; set; }
        public string Token { get; set; }
        public List<string> Roles { get; set; }
    }

}