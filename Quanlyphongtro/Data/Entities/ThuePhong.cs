﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Quanlyphongtro.Data.Entities
{
    public class ThuePhong
    {
        [Key]
        public int Id { get; set; }
        public string UserID { get; set; }
        public int Id_Phong { get; set; }
        [DataType(DataType.Date)]
        public DateTime NgayThue { get; set; }
        [DataType(DataType.Date)]
        public DateTime NgayThu { get; set; }
        public int GiaPhong { get; set; }
        public int TienDuaTruoc { get; set; }
        public int CSDDien { get; set; }
        public int CSCDien { get; set; }
        [StringLength(30)]
        public string KWDien { get; set; }
        public int GiaDien { get; set; }
        public int CSDNuoc { get; set; }
        public int CSCNuoc { get; set; }
        public int GiaNuoc { get; set; }
        public int IdTTThanhToan { get; set; }
        public int ThanhTien { get; set; }

        
    }
}
