﻿using System.ComponentModel.DataAnnotations;

namespace Quanlyphongtro.Data.Entities
{
    public class LienHe
    {
        [Key]  
        public int id_LienHe { get; set; }
        public string userID { get; set; }
        public int id_Phong { get; set; }
        public string NoiDung { get; set; }
    }
}
