﻿using System.ComponentModel.DataAnnotations;

namespace Quanlyphongtro.Data.Entities
{
    public class Phong
    {
        [Key] 
        public int Id_Phong { get; set; }

        [StringLength(30)]
        public string TenPhong { get; set; }
        public int MaTinhTrang { get; set; }
        public int Id_NhaTro { get; set; }
    }
}
