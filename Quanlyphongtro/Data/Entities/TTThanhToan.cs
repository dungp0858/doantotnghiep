﻿using System.ComponentModel.DataAnnotations;

namespace Quanlyphongtro.Data.Entities
{
    public class TTThanhToan
    {
        [Key]
        public int IdTTThanhToan { get; set; }

        [StringLength(20)]
        public string TrangThai { get; set; }

    }
}
