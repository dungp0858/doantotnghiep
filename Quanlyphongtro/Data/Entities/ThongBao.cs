﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Quanlyphongtro.Data.Entities
{
    public class ThongBao
    {
        [Key]
        public int Id_ThongBao { get; set; }

        [DataType(DataType.Date)]
        public DateTime NgayDang { get; set; }
        public string TieuDe { get; set; }

        public string NoiDung { get; set; }
    }
}
