﻿using System.ComponentModel.DataAnnotations;

namespace Quanlyphongtro.Data.Entities
{
    public class TinhTrang
    {
        [Key]
        public int MaTinhTrang { get; set; }
        [StringLength(20)]
        public string TrangThai { get; set; }
    }
}
