﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace Quanlyphongtro.Data.Entities
{
    public class User: IdentityUser
    {
        [StringLength(50)]
        public string Hovaten { get; set; }

        [DataType(DataType.Date)]
        public DateTime NgayDangKy { get; set; }
       
        public int SoDT { get; set; }
        [StringLength(200)]
        public string DiaChi { get; set; }


    }
}
