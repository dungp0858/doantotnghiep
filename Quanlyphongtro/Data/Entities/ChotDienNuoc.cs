﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Quanlyphongtro.Data.Entities
{
    public class ChotDienNuoc
    {
        [Key]   
        public int Id_ChotDN { get; set; }
        public string UserID { get; set; }
        [DataType(DataType.Date)]
        public DateTime NgayChot { get; set; }
        public string ImgDien { get; set; }
        public string ImgNuoc { get; set; }
    }
}
