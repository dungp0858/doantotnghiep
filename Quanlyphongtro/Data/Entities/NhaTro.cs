﻿using System.ComponentModel.DataAnnotations;

namespace Quanlyphongtro.Data.Entities
{
    public class NhaTro
    {
        [Key]   
        public int Id_NhaTro { get; set; }

        [StringLength(30)]
        public string ChuNha { get; set; }
        public int SoDT { get; set; }
        public string DiaChi { get; set; }

    }
}
