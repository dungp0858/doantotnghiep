﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Quanlyphongtro.Data.Entities;

namespace Quanlyphongtro.Data
{
    public class AppDbContext: IdentityDbContext<User, IdentityRole, string>
    {
        public DbSet<Phong> Phong { get; set; }
        public DbSet<NhaTro> NhaTro { get; set; }
        public DbSet<ThuePhong> ThuePhong { get; set; }
        public DbSet<ChotDienNuoc> ChotDienNuoc { get; set; }
        public DbSet<ThongBao> ThongBao { get; set; }
        public DbSet<TinhTrang> TinhTrang { get; set; }
        public DbSet<TTThanhToan> TTThanhToan { get; set; }
        public DbSet<LienHe> LienHe { get; set; }
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }
    }
}
